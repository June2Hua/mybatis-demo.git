package com.junehua;

import com.junehua.mapper.UserMapper;
import com.junehua.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MybatisDemo {

    @Test
    public void mybatisDemoTest() throws IOException {
        // Mybatis全局配置文件
        InputStream mybatisConfig = Resources.getResourceAsStream("mybatisConfig.xml");
        // 创建SqlSession工厂类
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(mybatisConfig);
        // 由工厂类创建SqlSession类
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 获取Mapper文件
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        // 查询并打印数据
        List<User> users = userMapper.listAll();
        users.forEach(System.out::println);
    }
}
