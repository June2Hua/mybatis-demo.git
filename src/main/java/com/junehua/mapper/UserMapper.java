package com.junehua.mapper;

import com.junehua.pojo.User;

import java.util.List;

public interface UserMapper {

    int insert(User user);

    List<User> listAll();
}
