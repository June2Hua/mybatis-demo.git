package com.junehua;

//STEP 1. Import required packages
import com.junehua.pojo.User;

import java.sql.*;

public class Main {
    // JDBC 信息
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/demo?useSSL=false&serverTimezone=UTC&useUnicode=true&characterEncoding=UTF-8";
    static final String USER = "root";
    static final String PASS = "C8uYdns?67ke";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try{
            //STEP 2: 注册JDBC Driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: 建立连接
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: 执行查询
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM user";
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 5: 封装结果集
            while(rs.next()){
                String id  = rs.getString("id");
                String name = rs.getString("age");
                Integer age = rs.getInt("age");
                Date createTime = rs.getDate("create_time");
                User user = new User(id, name, age, createTime);

                System.out.println(user);
                System.out.println("---------------");
            }
            //STEP 6: 关闭资源
            rs.close();
            stmt.close();
            conn.close();
        }catch(SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    stmt.close();
            } catch (SQLException se2){

            }
            try{
                if(conn!=null)
                    conn.close();
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
    }
}